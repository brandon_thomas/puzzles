#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct bitmap {
	uint32_t *arr;
	int x_dim;
};

/* Initialize a 2D bitmap, with all bits = false. */
static inline void init_bitmap(struct bitmap *b, const int x, const int y)
{
	int m = 1+(x*y/32);
	b->arr = malloc(sizeof(uint32_t)*m);
	b->x_dim = x;
	for (int i = 0; i < m; i++) {
		b->arr[i] = 0;
	}
}

/* Set a bit to true.*/
static inline void set_bit(struct bitmap b, const int x, const int y)
{
	int coord = x+(b.x_dim*y);
	b.arr[coord/32] |= 1<<(coord%32);
}

/* Get a value of a bit. */
static inline int get_bit(struct bitmap b, const int x, const int y)
{
	int coord = x+(b.x_dim*y);
	return b.arr[coord/32] & (1<<(coord%32));
}

/* Set minimum possible sum and the span. */
void minspan(const int n, const int *S, int *min, int *span)
{
	*min = 0;
	int max = 0;
	for (int i = 1; i <= n; i++) {
		if (S[i] < 0) {
			*min += S[i];
		} else {
			max += S[i];
		}
	}
	*span = max - *min;
}

/* Calculate F'(i, x). Return true if F'(i, 0). */
static inline int f_prime(const int i, const int x, const int min, const int *S, struct bitmap F)
{
	if (get_bit(F, i-1, x) ||
	    S[i]-min == x ||
	    get_bit(F, i-1, x-S[i])) {
		set_bit(F, i, x);
		return x == 0-min;
	}
}

/* Return true if solution exists. */
int solve_subset_sum_problem(const int n, const int *S)
{
	int min;
	int span;
	struct bitmap F;

	minspan(n, S, &min, &span);

	init_bitmap(&F, n+1, span+1);

	/* Fill bitmap with F'(i, x) */
	set_bit(F, 1, S[1]-min);
	for (int i = 2; i <= n; i++) {
		for (int x = 0; x <= span; x++) {
			if (f_prime(i, x, min, S, F)) {
				free(F.arr);
				return 1;
			}
		}
	}

	return 0;
}

int main()
{
	int n;
	int *S;

	/* Read n and S. */
	scanf("%d", &n);
	S = malloc(sizeof(int)*(n+1));
	for (int i = 1; i <= n; i++) {
		scanf("%d", &S[i]);
	}

	if (solve_subset_sum_problem(n, S)) {
		printf("Solution exists.\n");
	}
	else {
		printf("No solution exists.\n");
	}

	free(S);

	return 0;
}
