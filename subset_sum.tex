\documentclass[twocolumn]{article}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{listings}

\usepackage[libertine]{newtxmath}

\lstset{breaklines = true,
        language = c,
        showstringspaces = false,
        tabsize = 2}

\newtheorem{lemma}[section]{Lemma}

\begin{document}

\section*{Subset Sum Problem}

Given a set of integers, is there a non-empty subset whose sum is 0?

For example, given $\{-3, 1, 4, 2, 5, -9\}$, since the sum of $\{-3, 1, 2\}$ is 0, the answer is yes.

\section*{Algorithm}

This algorithm is an example of Dynamic Programming. Let $S := \{s_{1}, ..., s_{n}\}$ be the given set of integers. Define $F'(i, x) := \text{true}$ only if there is a non-empty subset of the first $i$ integers in $S$ whose sum is $x$. Thus, if $F'(n, 0)$ is true, the answer to the Subset Sum Problem is yes.

For example, if $S = \{-3, 1, 4, 2, 5, -9\}$, then $F'(3, -2)$ is true, because out of the first 3 integers, the sum of $\{-3, 1\}$ is -2.

A more precise description of $F'$ is needed. $F'(1, x) \equiv$ if $x$ is equal to $s_{1}$. $F'(i, x) \equiv$ if one of the following is true: $F'(i-1, x)$, $x$ is equal to $s_{i}$, or $F'(i-1,x-s_{i})$. See the proof of Lemma 2 for an explanation why.

Fill a bitmap $F[i][x] := F'(i, x)$, so $F'$ can reference previous values without recalculating. Fill each row, $i$, before moving to the next row, $i+1$. If $F'(i, 0)$ is true for any $i$, then $F'(n, 0)$ is true and the answer is yes. Note that in the C implementation, F cannot have negative indexes, so an offset will need to be added (this will not be accounted for in the proof or analysis, but the results are the same).

\section*{Correctness Proof}

\begin{proof}
Lemma 1, Lemma 2,  Lemma 3 and Lemma 4 are sufficient to show the algorithm is correct.
\end{proof}

\begin{lemma}
$F'(n, 0)$ is the answer to the subset sum problem.
\begin{proof}
Clearly by substitution, $F'(n, 0)$ is equivalent to the subset problem.
\end{proof}
\end{lemma}

\begin{lemma}
$F'(1, x) \equiv (x = s_{1})$ and $F'(i, x) \equiv F'(i-1, x) \vee (s_{i} = x) \vee F'(i-1, x - s_{i})$ is correct with the definition of $F'$.
\begin{proof}

Let $P(0)$ be the statement "$F'(1, x) \equiv (x = s_{1})$" and $P(j)$ be the statement "$F'(i, x) \equiv F'(i-1, x) \vee (s_{i} = x) \vee F'(i-1, x - s_{i})$, where ($i$, $x$) corresponds to the $j^{th}$ iteration".

By substitution, $F'(1, x)$ is only true if $x = s_{1}$, thus $P(0)$ is true. Assume $P(k)$ holds true for all $k < j$ (thus row $i-1$ has been computed correctly). Consider $P(j)$; $F'(i, x)$ is true if at least one of the following is true:
\begin{enumerate}
\item $F'(i-1,x)$: since $x$ can be reached with the first $i-1$ integers in $S$, it can be reached with the first $i$ integers in $S$.
\item $s_{i} = x$, by substitution.
\item $F'(i-1, x-s_{i})$: since $x-s_{i}$ can be reached with the first $i-1$ integers in $S$, then $x$ can be reached with the first $i$ integers in $S$ by adding $s_{i}$.
\end{enumerate}
These are the only possible cases, since every combination of using "$s_{i}$" and "the first $i-1$ integers in $S$" has been considered. This yields the recurrence relation, thus $P(j)$ is true. Therefore, by strong mathematical induction, $P(j)$ is true for all $j$ and the recurrence satisfies our definition of $F'(i, x)$.

\end{proof}
\end{lemma}

\begin{lemma}
$F'(i, 0) \Rightarrow F'(n, 0)$
\begin{proof}
If $F'(i, 0)$, then 0 is the sum of a subset of the first $i$ elements of $S$, which is also a subset of $S$, meeting the definition of $F'(n, 0)$.
\end{proof}
\end{lemma}

\begin{lemma}
$F[i-1][x]$ and $F[i-1][x-s_{i}]$ has been calculated before $F[i][x]$ is calculated.
\begin{proof}
If row $i$ is being processed, then all $x$ in row $i-1$ has been calculated. Therefore, $F[i-1][x]$ and $F[i-1][x-s_{i}]$ have been calculated.
\end{proof}
\end{lemma}

\section*{Analysis}
Let span($S$) := (maximum sum of a subset of $S$) - (minimum sum of a subset of $S$).

Space complexity: The only axillary memory allocated is a ($n$ x span($S$)) size bitmap. Thus, the worse case space complexity is $\Theta(n \ \text{span}(S))$.

Time complexity:  The recurrence is calculated in constant time, and is computed at most once for each member of the ($n$ x span($S$)) size bitmap. Thus, the worst case time complexity is $\Theta(n \ \text{span}(S))$.

\section*{C Implementation}
\lstinputlisting{subset_sum.c}

\end{document}
