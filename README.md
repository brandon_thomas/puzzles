puzzles
=======

Some puzzles solved with Mathematics, Computer Science and Programming.

Generated PDF's:

[Subset Sum Problem](https://bitbucket.org/brandon_thomas/puzzles/downloads/subset_sum.pdf)